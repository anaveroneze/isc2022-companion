#!/bin/bash

# File to pre process the output data

#---------------------------------
# Pre process log data
PATHFILE="$(pwd)/scripts/../output/final"
FILENAME=full-lenet.csv

if [[ -f $PATHFILE/$FILENAME ]]; then
    rm $PATHFILE/$FILENAME
fi

if [[ -f $PATHFILE/*csv ]]; then
    rm $PATHFILE/*csv
fi


for dir in ${PATHFILE}/*; do
    tempvalues=`echo $dir | sed 's/.*\///'`
    machine=`echo $tempvalues | cut -d "-" -f1`
    framework=`echo $tempvalues | cut -d "-" -f2`

    # Getting training information for each log file
    for filename in $dir/*.log; do
	tempvalues=`echo $filename | sed 's/.*output//'`
	time=`cat $filename | grep -oP "time:.*" | cut -d' ' -f2-`
	loss=`cat $filename | grep -oP "loss.*" | cut -d' ' -f2- `
	accuracy=`cat $filename | grep -oP "accuracy.*" | cut -d' ' -f2- `
	batch=`echo $tempvalues | sed 's/.*\///; s/-.*//'`
	gpus=`echo $tempvalues | sed -e 's/.*-\(.*\)-.*/\1/'`
	rep=`echo $tempvalues | sed 's/.*-//; s/.log.*//'`
	startscript=`cat $filename | grep -oP "Start script:.*" | cut -d' ' -f3-`
	endscript=`cat $filename | grep -oP "End script:.*" | cut -d' ' -f3-`
	trainloss=`cat $filename | grep -oP "\[.*" | cut -d',' -f100 | sed 's/.$//' | head -n 1`
	trainaccuracy=`cat $filename | grep -oP "\[.*" | cut -d',' -f100 | sed 's/.$//' | tail -n 1`
	echo "$framework,$machine,$gpus,$time,$loss,$accuracy,$trainloss,$trainaccuracy,$batch,100,$rep,$startscript,$endscript" >> $PATHFILE/$FILENAME
    done
done

# Pre-process data to the second image
batch=100
FILENAME=callbacks-100.csv
if [[ -f $PATHFILE/$FILENAME ]]; then
    rm $PATHFILE/$FILENAME
fi

declare -a FrameworksNames=("${PATHFILE}/*-tarantella" "${PATHFILE}/*-horovod")

echo "machine,framework,gpus,rep,event.type,rank,time.temp,operation,type,parameter,garbage" >> $PATHFILE/$FILENAME

for dir in ${FrameworksNames[@]}; do
    tempvalues=`echo $dir | sed 's/.*\///'`
    machine=`echo $tempvalues | cut -d "-" -f1`
    framework=`echo $tempvalues | cut -d "-" -f2`
	{
	for file in $dir/$batch-*/logfile*; do
	    tempvalues=`echo $file | sed 's/.*output//'`
	    gpus=`echo $tempvalues | cut -d "-" -f3`
	    rep=`echo $tempvalues | cut -d "-" -f4 | sed 's/\/.*//'`
	    cat $file | grep on_epoch | sed "s/^/${machine},${framework},${gpus},${rep},/"
	done
	} >> $PATHFILE/$FILENAME
done

#------------------------------
# Pre-process data for the Keras callbacks + NVProf plot
PATHFILE="$(pwd)/scripts/../output/nvprof-grid/"
for file in $PATHFILE/*/nvprof*.csv; do
    if [ -f "$file-original" ]; then
	    echo "File already processed."
	else
	    cp $file $file-original
	    sed -i '/^==/d' $file
	    sed -i '1,2d' $file
    fi
done

#------------------------------
FILENAME=output.csv
if [[ -f $PATHFILE/$FILENAME ]]; then
 rm $PATHFILE/$FILENAME
fi

echo "Start,Duration,GridX,GridZ,GridY,BlockX,BlockZ,BlockY,RegistersPerThread,StaticSMem,DynamicSMem,Size,Throughput,SrcMemType,DstMemType,Device,Context,Stream,Name,Correlation,Id,Framework" >> $PATHFILE/$FILENAME
{
for file in $PATHFILE/*/*.csv; do
    tempvalues=`echo $file | sed 's/.*output//'`
    id=`echo $tempvalues | sed 's/.*-//;s/.csv.*//'`
    framework=`echo $tempvalues | cut -d\/ -f4 | cut -d "-" -f4`
    sed "s/$/,$id,$framework/g" $file
done
} >> $PATHFILE/$FILENAME

#------------------------------
FILENAME=callbacks.csv
if [[ -f $PATHFILE/$FILENAME ]]; then
    rm $PATHFILE/$FILENAME
fi
machine=gemini
echo "machine,framework,event.type,rank,time.temp,operation,type,parameter,garbage" >> $PATHFILE/$FILENAME
{
    for file in $PATHFILE/*/logfile*; do
            tempvalues=`echo $file | sed 's/.*output//'`
            framework=`echo $tempvalues | cut -d\/ -f4 | cut -d "-" -f4`
	    cat $file | grep on_train | sed "s/^/${machine},${framework},/"
	    cat $file | grep on_epoch | sed "s/^/${machine},${framework},/"
	    cat $file | grep on_test | sed "s/^/${machine},${framework},/"
    done
} >> $PATHFILE/$FILENAME

