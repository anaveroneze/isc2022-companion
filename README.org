# -- org-startup-with-inline-images: nil --
#+TITLE: Companion Material - ISC22
#+AUTHOR: Solorzano, Schnorr
#+LATEX_HEADER: \usepackage[margin=2cm,a4paper]{geometry}
#+STARTUP: overview indent noinlineimages
#+TAGS: noexport(n) deprecated(d)
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+SEQ_TODO: TODO(t!) STARTED(s!) WAITING(w!) | DONE(d!) CANCELLED(c!) DEFERRED(f!)

* Introduction

Companion Material for the paper ``Understanding Distributed Deep
Learning Performance by Correlating HPC and Machine Learning
Measurements'', by Ana Luisa Veroneze Solórzano and Lucas Mello 
Schnorr. Approved for the [[https://www.isc-hpc.com/research-papers-2022.html][ISC High Performance 2022]].

You can clone this repository with the following command:
#+begin_src shell :session *shell* :results output :exports both 
git clone https://gitlab.com/anaveroneze/isc22-companion.git
#+end_src

To download and extract only the output files processed with our
scripts you can use:
#+begin_src shell :results output :exports both
wget https://gitlab.com/anaveroneze/isc2022-companion/-/raw/main/output.tar.gz
tar -xzvf output.tar.gz
#+end_src

- The [[./img]] directory contains the expected Figures for our code
  snippets
- The [[./output]] directory contains all dataset obtained with
  experiments for Horovod and Tarantella used to this analysis
- The [[./scripts]] directory contains the code snippets used to our
  performance analysis and generate the figures

* Requirements

The code and data manipulation were written in the R programming
language using the [[https://cran.rstudio.com/web/packages/tidyverse/][tidyverse]], a collection of R packages for data
science.

The package [[https://cran.r-project.org/web/packages/magick/][magick]] is used to generate the figures processed with R,
the package [[https://cran.r-project.org/web/packages/data.table/][data.table]] is used to read CSV returning the number of
full items successfully read. We also use the [[https://cran.rstudio.com/web/packages/cowplot/][cowplot]] package to
combine multiple ggplots into the same graphic, the [[https://db.rstudio.com/databases/sqlite/][RSQLite]] and [[https://dbi.r-dbi.org/][DBI]]
package to manipulate SQLite files, [[https://cran.rstudio.com/web/packages/ggforce/][ggforce]] for zooming into one facet
region, [[https://cran.r-project.org/web/packages/lubridate/][lubridate]] to deal with dates conversion, and [[https://cran.rstudio.com/web/packages/ggh4x][ggh4x]] to resize
some panels with facets.

Run the snippet down below to download all:

#+begin_src shell :results output :exports both
install.packages(c("tidyverse", "data.table", "magick", "RSQLite", "cowplot", "DBI", "ggforce", "lubridate", "ggh4x"))
#+end_src

Note that the package magick may need to install the libmagick in the
system:
#+begin_src shell :results output :exports both
sudo apt-get install libmagick++-dev
#+end_src

Pre-process the data by running the script [[./scripts/process_data.sh]]:
#+begin_src shell :results output :exports both
chmod +x scripts/process_data.sh
./scripts/process_data.sh
#+end_src

This command may take a while to finish.

Then you can generate each figure in the paper separately using the
command:
#+begin_src shell :results output :exports both
for FIG in $(seq 1 8); do
    Rscript ./scripts/figure${FIG}.R
done
#+end_src

They will be generated in the PNG file format and stored in the
directory [[./img]]

* Authors
- Ana Luisa Veroneze Solórzano ([[https://www.inf.ufrgs.br/~alvsolorzano][https://www.inf.ufrgs.br/~alvsolorzano]]) - Informatics Institute/UFRGS, Brazil
- Lucas Mello Schnorr ([[http://www.inf.ufrgs.br/~schnorr][http://www.inf.ufrgs.br/~schnorr]]) - Informatics Institute/UFRGS, Brazil
